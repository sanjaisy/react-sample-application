﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactSampleApplication.Models
{
    public class ProfessionalLearnigViewModel
    {
        public string OwnedBy { get; set; } = "Anand Jaisy";
        public string CreateByFullName { get; set; } = "Anand Jaisy";
        public string CourseName { get; set; }
        public string Provider { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public decimal? RegistrationExpenses { get; set; }
        public decimal? AccommodationExpenses { get; set; }
        public decimal? TravelExpenses { get; set; }
        public string ExpenseComment { get; set; }
        public bool IsExternal { get; set; }
        public Guid Id { get; set; }

    }
}
