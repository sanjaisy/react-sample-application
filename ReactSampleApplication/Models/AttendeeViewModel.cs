﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactSampleApplication.Models
{
    public class AttendeeViewModel
    {
        public string name { get; set; }
        public object phoneNumber { get; set; }
        public string emailAddress { get; set; }
        public string eventId { get; set; }
        public string id { get; set; }
        public bool active { get; set; }
        public string modifiedBy { get; set; }
        public DateTime modifiedOn { get; set; }
        public string createdBy { get; set; }
        public DateTime createdOn { get; set; }
    }
}
