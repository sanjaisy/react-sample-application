﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ReactSampleApplication.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ReactSampleApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserManagementController : ControllerBase
    {
        string path = @"C:\Users\san\source\repos\ReactSampleApplication\ReactSampleApplication\temp\UserManagement.json";
        // GET: api/UserManagement
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var value = await ReadTheFile();
            return Ok(value ?? new List<UserManagementViewModel>());
        }

        // GET: api/UserManagement/5
        [HttpGet("{id}", Name = "GetUserManagementById")]
        public async Task<IActionResult> GetUserManagementById(Guid id)
        {
            var data = await ReadTheFile();
            return Ok(data.FirstOrDefault(x => x.Id == id));
        }

        // POST: api/UserManagement
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserManagementViewModel model)
        {
            await PostData(model);
            return Ok();
        }

        // PUT: api/UserManagement/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UserManagementViewModel model)
        {
            if (ModelState.IsValid)
            {
                var items = await ReadTheFile();
                var itemIndex = items.FindIndex(x => x.Id == id);
                var item = items.ElementAt(itemIndex);
                item.Email = model.Email;
                item.FirstName = model.FirstName;
                item.LastName = model.LastName;
                var jsonObject = JsonConvert.SerializeObject(items);
                await UpdateValue(jsonObject);
            }
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var item = await ReadTheFile();

            var itemToRemove = item.SingleOrDefault(x => x.Id == id);
            if (itemToRemove != null)
                item.Remove(itemToRemove);
            var jsonObject = JsonConvert.SerializeObject(item);
            await UpdateValue(jsonObject);
            return Ok();
        }



        private async Task PostData(UserManagementViewModel userManagement)
        {
            userManagement.Id = Guid.NewGuid();
            var item = await ReadTheFile();
            item.Add(userManagement);
            var jsonObject = JsonConvert.SerializeObject(item);
            if (System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path))
                {
                    await sw.WriteLineAsync(jsonObject);
                }
            }
        }



        private async Task<List<UserManagementViewModel>> ReadTheFile()
        {
            // Open the file to read from.
            using (StreamReader sr = System.IO.File.OpenText(path))
            {
                string values = await sr.ReadToEndAsync();
                var deserializeObject = JsonConvert.DeserializeObject<List<UserManagementViewModel>>(values);
                return deserializeObject ?? new List<UserManagementViewModel>();
            }
        }

        private async Task UpdateValue(string value)
        {
            if (System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path))
                {
                    await sw.WriteLineAsync(value);
                }
            }
        }
    }
}
