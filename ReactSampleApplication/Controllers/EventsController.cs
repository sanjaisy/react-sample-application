﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ReactSampleApplication.Models;

namespace ReactSampleApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        string path = @"C:\Users\san\source\repos\ReactSampleApplication\ReactSampleApplication\temp\Event.json";
        // GET: api/Events
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var value = await ReadTheFile();
            return Ok(value ?? new List<EventViewModel>());
        }

        // GET: api/Events/5
        [HttpGet("{id}", Name = "GetEventsById")]
        public async Task<IActionResult> GetEventsById(Guid id)
        {
            var data = await ReadTheFile();
            return Ok(data.FirstOrDefault(x => x.Id == id));
        }

        // POST: api/Events
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EventViewModel model)
        {
            await PostData(model);
            return Ok();
        }

        // PUT: api/Events/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] EventViewModel model)
        {
            if (ModelState.IsValid)
            {
                var items = await ReadTheFile();
                var itemIndex = items.FindIndex(x => x.Id == id);
                var item = items.ElementAt(itemIndex);
                item.AttendeeLimit = model.AttendeeLimit;
                item.ContactEmail = model.ContactEmail;
                item.ContactName = model.ContactName;
                item.ContactPhone = model.ContactPhone;
                item.Cost = model.Cost;
                item.EventDescription = model.EventDescription;
                item.EventEndDateTime = model.EventEndDateTime;
                item.EventName = model.EventName;
                item.EventShortDescription = model.EventShortDescription;
                item.EventStartDateTime = model.EventStartDateTime;
                item.WebUrl = model.WebUrl;
                item.Presenter = model.Presenter;
                var jsonObject = JsonConvert.SerializeObject(items);
                await UpdateValue(jsonObject);
            }
            return Ok();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var item = await ReadTheFile();
            var itemToRemove = item.SingleOrDefault(x => x.Id == id);
            if (itemToRemove != null)
                item.Remove(itemToRemove);
            var jsonObject = JsonConvert.SerializeObject(item);
            await UpdateValue(jsonObject);
            return Ok();
        }

        private async Task<List<EventViewModel>> ReadTheFile()
        {
            // Open the file to read from.
            using (StreamReader sr = System.IO.File.OpenText(path))
            {
                string values = await sr.ReadToEndAsync();
                var deserializeObject = JsonConvert.DeserializeObject<List<EventViewModel>>(values);
                return deserializeObject ?? new List<EventViewModel>();
            }
        }

        private async Task PostData(EventViewModel eventViewModel)
        {
            eventViewModel.Id = Guid.NewGuid();
            var item = await ReadTheFile();
            item.Add(eventViewModel);
            var jsonObject = JsonConvert.SerializeObject(item);
            if (System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path))
                {
                    await sw.WriteLineAsync(jsonObject);
                }
            }
        }

        private async Task UpdateValue(string value)
        {
            if (System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path))
                {
                    await sw.WriteLineAsync(value);
                }
            }
        }
    }
}
