﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ReactSampleApplication.Models;

namespace ReactSampleApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessionalLearningController : ControllerBase
    {
        string path = @"C:\Users\san\source\repos\ReactSampleApplication\ReactSampleApplication\temp\ProfessionalLearning.json";
        // GET: api/ProfessionalLearning
        [HttpGet]
        public async Task<IActionResult> Get(int startDateIndex)
        {
            var value = await ReadTheFile();
            return Ok(value ?? new List<ProfessionalLearnigViewModel>());
        }

        // GET: api/ProfessionalLearning/5
        [HttpGet("{id}", Name = "GetProfessionalLearningById")]
        public async Task<IActionResult> GetProfessionalLearningById(Guid id)
        {
            var data = await ReadTheFile();
            return Ok(data.FirstOrDefault(x => x.Id == id));
        }

        // POST: api/ProfessionalLearning
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProfessionalLearnigViewModel model)
        {
            await PostData(model);
            return Ok();

        }

        // PUT: api/ProfessionalLearning/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] ProfessionalLearnigViewModel model)
        {
            if (ModelState.IsValid)
            {
                var items = await ReadTheFile();
                var itemIndex = items.FindIndex(x => x.Id == id);
                var item = items.ElementAt(itemIndex);
                item.AccommodationExpenses = model.AccommodationExpenses;
                item.CourseName = model.CourseName;
                item.EndDateTime = model.EndDateTime;
                item.ExpenseComment = model.ExpenseComment;
                item.IsExternal = model.IsExternal;
                item.Provider = model.Provider;
                item.RegistrationExpenses = model.RegistrationExpenses;
                item.StartDateTime = model.StartDateTime;
                var jsonObject = JsonConvert.SerializeObject(items);
                await UpdateValue(jsonObject);
            }
            return Ok();

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var item = await ReadTheFile();

            var itemToRemove = item.SingleOrDefault(x => x.Id == id);
            if (itemToRemove != null)
                item.Remove(itemToRemove);
            var jsonObject = JsonConvert.SerializeObject(item);
            await UpdateValue(jsonObject);
            return Ok();
        }


        private async Task PostData(ProfessionalLearnigViewModel professionalLearnig)
        {
            professionalLearnig.Id = Guid.NewGuid();
            var item = await ReadTheFile();
            item.Add(professionalLearnig);
            var jsonObject = JsonConvert.SerializeObject(item);
            if (System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path))
                {
                    sw.WriteLine(jsonObject);
                }
            }
        }


        private async Task<List<ProfessionalLearnigViewModel>> ReadTheFile()
        {
            // Open the file to read from.
            using (StreamReader sr = System.IO.File.OpenText(path))
            {
                string values = await sr.ReadToEndAsync();
                var deserializeObject = JsonConvert.DeserializeObject<List<ProfessionalLearnigViewModel>>(values);
                return deserializeObject ?? new List<ProfessionalLearnigViewModel>();
            }
        }


        private async Task UpdateValue(string value)
        {
            if (System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(path))
                {
                    await sw.WriteLineAsync(value);
                }
            }
        }
    }
}
