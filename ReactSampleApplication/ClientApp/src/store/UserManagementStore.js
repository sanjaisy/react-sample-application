const requestWeatherForecastsType = 'REQUEST_WEATHER_FORECASTS';
const receiveWeatherForecastsType = 'RECEIVE_WEATHER_FORECASTS';

const initialState = { users: [], isLoading: false };

export const actionCreators = {
  requestWeatherForecasts: startDateIndex => async (dispatch, getState) => {
    dispatch({ type: requestWeatherForecastsType, startDateIndex });

    const url = `api/UserManagement`;
    const response = await fetch(url);
    const users = await response.json();

    dispatch({ type: receiveWeatherForecastsType, startDateIndex, users });
  },

  filterUserManagementById: id => async (dispatch) => {
    const url = `api/UserManagement/${id}`;
    const response = await fetch(url);
    return await response.json();
  },
  UserManagementFormsubmit: (id,modelValues) => async () => {
    if (id != undefined || id != null) {
      const url = `api/UserManagement/${id}`;
      return await fetch(url, {
        method: 'PUT',
        body: JSON.stringify(modelValues),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      });
    } else {
      const url = `api/UserManagement`;
      return await fetch(url, {
        method: 'POST',
        body: JSON.stringify(modelValues),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      });
    }
  },
  deleteUserManagement: (id) => async (dispatch) => {
    const url = `api/UserManagement/${id}`;
    return await fetch(url, { method: 'DELETE', headers: { 'Content-Type': 'application/json; charset=UTF-8' } });
  }
};

export const reducer = (state, action) => {
  state = state || initialState;

  switch (action.type) {
    case requestWeatherForecastsType:
      return {
        ...state,
        startDateIndex: action.startDateIndex,
        isLoading: true
      };

    case receiveWeatherForecastsType:
      return {
        ...state,
        startDateIndex: action.startDateIndex,
        users: action.users,
        isLoading: false
      };
  }

  return state;
};
