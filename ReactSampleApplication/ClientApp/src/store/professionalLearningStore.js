import ReduxConstants from '../Common/ReduxConstants';

export const actionCreators = {
  requestProfessionalLeaning: startDateIndex => async (dispatch, getState) => {

    dispatch({ type: ReduxConstants.requestProfessionalLearningType, startDateIndex });

    const url = `api/ProfessionalLearning?startDateIndex=${startDateIndex}`;
    const response = await fetch(url);
    const professionalLearnings = await response.json();

    dispatch({ type: ReduxConstants.receiveProfessionalLearningType, startDateIndex, professionalLearnings });
  },

  deleteProfessionalLeaning: (id) => async (dispatch) => {
    const url = `api/ProfessionalLearning/${id}`;
    return await fetch(url, { method: 'DELETE', headers: { 'Content-Type': 'application/json; charset=UTF-8' } });
  }
};
