const requestEventsType = 'REQUEST_EVENTS';
const receiveEventsType = 'RECEIVE_EVENTS';

const requestEventType = 'REQUEST_EVENT';
const receiveEventType = 'RECEIVE_EVENT';


const resetEventValue = 'RESET_EVENT_VALUE';
const updateEventValue = 'UPDATE_EVENT_VALUE';


const initialState = { Events: [], Event: {}, isLoading: false };

export const actionCreators = {
  requestEvents: startDateIndex => async (dispatch, getState) => {
    dispatch({ type: requestEventsType, startDateIndex });
    const url = `api/Events`;
    const response = await fetch(url);
    const events = await response.json();

    dispatch({ type: receiveEventsType, startDateIndex, events });
  },

  requestEvent: id => async (dispatch) => {
    dispatch({ type: requestEventType, id });
    const url = `api/Events/${id}`;
    const response = await fetch(url);
    const event = await response.json();
    dispatch({ type: receiveEventType, id, event });
  },

  resetEvent: () => ({ type: resetEventValue }),


  updateEventValue: eventItem => async (dispatch, getState) => {
    let event = getState().EventManagement.Event;
    event[eventItem.target.name] = eventItem.target.value;
    dispatch({ type: updateEventValue, event });
  },


  eventSubmit: id => async (dispatch, getState) => {
    let user = getState().EventManagement.Event;
    if (id != undefined || id != null) {
      const url = `api/Events/${id}`;
      return await fetch(url, {
        method: 'PUT',
        body: JSON.stringify(user),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      });
    } else {
      const url = `api/Events`;
      return await fetch(url, {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8'
        }
      });
    }
  },



  deleteEvent: (id) => async (dispatch) => {
    const url = `api/Events/${id}`;
    return await fetch(url, { method: 'DELETE', headers: { 'Content-Type': 'application/json; charset=UTF-8' } });
  }

};

export const reducer = (state, action) => {
  state = state || initialState;

  switch (action.type) {
    case requestEventsType:
      return {
        ...state,
        startDateIndex: action.startDateIndex,
        isLoading: true
      };
    case receiveEventsType:
      return {
        ...state,
        startDateIndex: action.startDateIndex,
        Events: action.events,
        isLoading: false
      };

    case requestEventType:
      return {
        ...state,
        startDateIndex: action.startDateIndex,
        isLoading: true
      };
    case receiveEventType:
      return {
        ...state,
        startDateIndex: action.startDateIndex,
        Event: action.event,
        isLoading: false
      };
    case resetEventValue:
      return {
        ...state,
        startDateIndex: action.startDateIndex,
        Event: {},
        isLoading: false
      };

    case updateEventValue:
      return {
        ...state,
        startDateIndex: action.startDateIndex,
        Event: Object.assign({}, action.event),
        isLoading: false
      };
  }
  return state;
};
