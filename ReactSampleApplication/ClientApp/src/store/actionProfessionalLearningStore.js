import ReduxConstants from '../Common/ReduxConstants';
export const actionCreators = {

    filterProfessionalLeaningByid: id => async (dispatch, getState) => {
        if (id === getState().professionalLearning.id) {
            // Don't issue a duplicate request (we already have or are loading the requested data)
            return;
        }
        dispatch({ type: ReduxConstants.requestFilterProfessionalLearningTypeId, id });

        const url = `api/ProfessionalLearning/${id}`;
        const response = await fetch(url);
        const professionalLearning = await response.json();

        dispatch({ type: ReduxConstants.receiveFilterProfessionalLearningId, id, professionalLearning });
    },

    resetProfessionalLeaning: () => ({ type: ReduxConstants.requestFilterProfessionalLearningTypeId }),

    updateprofessionalLearningValue: professionalLearningItem => async (dispatch, getState) => {
        let professionalLearning = getState().professionalLearning.professionalLearning;
        if (professionalLearningItem.target.type === 'checkbox') {
            professionalLearning[professionalLearningItem.target.name] = professionalLearningItem.target.checked;
        } else {
            professionalLearning[professionalLearningItem.target.name] = professionalLearningItem.target.value;
        }
        dispatch({ type: ReduxConstants.recieveProfessionalLearningUpdatecValue, professionalLearning });
    },

    professionalLearningFormsubmit: id => async (dispatch, getState) => {
        let professionalLearning = getState().professionalLearning.professionalLearning;
        if (id != undefined || id != null) {
            const url = `api/ProfessionalLearning/${id}`;
            return await fetch(url, {
                method: 'PUT',
                body: JSON.stringify(professionalLearning),
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                }
            });
        } else {
            const url = `api/ProfessionalLearning`;
            return await fetch(url, {
                method: 'POST',
                body: JSON.stringify(professionalLearning),
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                }
            });
        }
    }
};