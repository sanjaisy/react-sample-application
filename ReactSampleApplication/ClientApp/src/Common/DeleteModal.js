import React, {Component} from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class DeleteModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.close = this.close.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  deleteItem(){
    this.toggle()
    this.props.clickHandler()
  }

  close(){
    this.toggle()
    this.props.onClose()
  }


  render() {
    return (
      <div>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>Delete the item</ModalHeader>
          <ModalBody>
            Do you want to delete the item ?
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.deleteItem}>yes</Button>
            <Button color="secondary" onClick={this.close}>No</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default DeleteModal;