
const ReduxConstants = {
    requestProfessionalLearningType: 'REQUEST_PROFESSIONAL_LEARNING',
    receiveProfessionalLearningType : 'RECEIVE_PROFESSIONAL_LEARNING',

    requestFilterProfessionalLearningTypeId : 'REQUEST_PROFESSIONAL_LEARNING_BY_ID',
    receiveFilterProfessionalLearningId : 'FILTER_PROFESSIONAL_LEARNING_BY_ID',

    recieveProfessionalLearningUpdatecValue : 'RECEIVE_PROFESSIONAL_LEARNING_UPDATE_VALUE'
}

export default ReduxConstants