import React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/ProfessionalLearnig/Home';
import Counter from './components/Events/Event';
import FetchData from './components/UserManagement/UserManagement';
import ProfessionalLearningAction from './components/ProfessionalLearnig/ActionProfessionalLearnig';
import UsermanagementAction from './components/UserManagement/AddEditUsersAction';
import EventsAction from './components/Events/AddEditEventAction';


export default () => (
  <Layout>
    <Route exact path='/' component={Home} />
    <Route exact path='/professionallearning' component={ProfessionalLearningAction} />
    <Route exact path='/professionallearning/:id' component={ProfessionalLearningAction} />
    <Route exact path='/event' component={Counter} />
    <Route exact path='/actionEvent' component={EventsAction} />
    <Route exact path='/actionEvent/:id' component={EventsAction} />
    <Route path='/fetch-data/:startDateIndex?' component={FetchData} />
    <Route exact path='/usermanagement' component={UsermanagementAction} />
    <Route exact path='/usermanagement/:id' component={UsermanagementAction} />
  </Layout>
);
