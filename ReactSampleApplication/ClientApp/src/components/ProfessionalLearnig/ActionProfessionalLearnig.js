import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/actionProfessionalLearningStore';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

class ProfessionalLearningAction extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    // This method is called when the component is first added to the document
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    this.id = this.props.match.params.id;
    if (this.id == undefined || this.id == null) {
      this.props.resetProfessionalLeaning();
    } else {
      this.props.filterProfessionalLeaningByid(this.id);
    }
  }
  handleSubmit = async (event) => {
    event.preventDefault();
    await this.props.professionalLearningFormsubmit(this.id);
    this.props.history.push('/');
    
  }


  render() {
    return (
      <div className="container mb-5">
        <h1>Edit/Add Professional Learning</h1>
        <p>This component demonstrates Add/Edit data from the server and working with URL parameters.</p>
        <br /><br />
        <RenderProfessionalLearningTable {...this.props} handleSubmit={this.handleSubmit} />
      </div>
    );
  }
}


function RenderProfessionalLearningTable(props) {
  return (
    <form className="container" onSubmit={props.handleSubmit}>
      <div className="form-row">
        <div className="form-group col-sm-6">
          <label>Course Name *</label>
          <input type="text" className="form-control" name="courseName" value={props.professionalLearning.courseName || ''} onChange={props.updateprofessionalLearningValue}
            aria-describedby="Course Name" placeholder="Enter a course name" />
        </div>
        <div className="form-group col-sm-6">
          <label>Provider *</label>
          <input type="text" className="form-control" name="provider" value={props.professionalLearning.provider || ''} onChange={props.updateprofessionalLearningValue}
            id="provider" aria-describedby="Provider" placeholder="Enter a Provider" />
        </div>
      </div >
      <br />

      <div className="form-row">
        <div className="form-group col-sm-6">
          <label>Start date *</label>
          <input type="text" className="form-control" name="startDateTime" value={props.professionalLearning.startDateTime || ''} onChange={props.updateprofessionalLearningValue}
            aria-describedby="Course Name" placeholder="Enter a Start Date" />
        </div>
        <div className="form-group col-sm-6">
          <label>End Date *</label>
          <input type="text" className="form-control" name="endDateTime" value={props.professionalLearning.endDateTime || ''} onChange={props.updateprofessionalLearningValue}
            id="provider" aria-describedby="Provider" placeholder="Enter a End Date" />
        </div>
      </div >
      <br />

      <div className="form-row">
        <div className="form-group col-sm-6">
          <label>Registration Expense *</label>
          <input type="text" className="form-control" name="registrationExpenses" value={props.professionalLearning.registrationExpenses || ''} onChange={props.updateprofessionalLearningValue}
            aria-describedby="Course Name" placeholder="Enter a Registration expense" />
        </div>
        <div className="form-group col-sm-6">
          <label>Owned By *</label>
          <div>{props.professionalLearning.ownedBy || ''}</div>
        </div>
      </div >
      <br />
      <div className="form-row">
        <div className="form-group col-sm-6">
          <div className="form-check">
            <input className="form-check-input" name="isExternal" type="checkbox" defaultChecked={props.professionalLearning.isExternal || false} id="defaultCheck1" onChange={props.updateprofessionalLearningValue} />
            <label className="form-check-label">
              Is External</label>
          </div>
        </div>
        <div className="form-group col-sm-6">
          <label>Accomodation Expense *</label>
          <input type="text" className="form-control" name="accommodationExpenses" value={props.professionalLearning.accommodationExpenses || ''} onChange={props.updateprofessionalLearningValue}
            id="provider" aria-describedby="Provider" placeholder="Enter a Accomodation Expense" />
        </div>
      </div >
      <br />
      <div className="form-row">
        <div className="form-group col-sm-6">
          <label>Travel Expense *</label>
          <input type="text" className="form-control" name="travelExpenses" value={props.professionalLearning.travelExpenses || ''} onChange={props.updateprofessionalLearningValue}
            aria-describedby="Course Name" placeholder="Enter a Travel Expense" />
        </div>
        <div className="form-group col-sm-6">
          <label>Expense Comment *</label>
          <input type="text" className="form-control" name="expenseComment" value={props.professionalLearning.expenseComment || ''} onChange={props.updateprofessionalLearningValue}
            id="provider" aria-describedby="Provider" placeholder="Enter a Expense Comment" />
        </div>

      </div >
      <br />
      <div className="d-flex justify-content-end">
        <Link type="button" className='btn btn-secondary' to={`/`}>Cancel</Link> &nbsp;&nbsp;
        <input type="submit" className='btn btn-primary' value="Submit" />
      </div>

    </form >
  );
}

export default connect(
  state => state.professionalLearning,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(ProfessionalLearningAction);