import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../../store/professionalLearningStore';
import { bindActionCreators } from 'redux';
import Moment from 'moment';


class Home extends Component {
  constructor(props) {
    super(props);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
  }
  componentDidMount() {
    // This method is called when the component is first added to the document
    this.ensureDataFetched();
  }
  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.ensureDataFetched();
    }
  }

  ensureDataFetched() {
    const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
    this.props.requestProfessionalLeaning(startDateIndex);
  }

  handleDeleteClick = async (id) => {
    await this.props.deleteProfessionalLeaning(id);
    this.ensureDataFetched();
  }
  render() {
    return (
      <div>
        <h1>Professional Learning</h1>
        <p>This component demonstrates fetching data from the server and working with URL parameters.</p>
        <RenderProfessionalLearningTable {...this.props} handleDeleteClick={this.handleDeleteClick} />
        {renderPagination(this.props)}
      </div>
    );
  }
}


function RenderProfessionalLearningTable(props) {
  return (
    <div>
      <div className="d-flex flex-row-reverse bd-highlight p-2">
        <Link type="button" className="btn btn-danger" to={`/professionallearning`}>Add PL</Link>
      </div>

      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Course Name</th>
            <th>provider</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Registration Expense</th>
            <th>Owned By</th>
            <th>External</th>
            <th>Accomodation Expense</th>
            <th>Travel Expense</th>
            <th>Expense Comment</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {props.professionalLearnings.map(professionalLearnig =>
            <tr key={professionalLearnig.id}>
              <td>{professionalLearnig.courseName}</td>
              <td>{professionalLearnig.provider}</td>
              <td>{Moment(professionalLearnig.startDateTime).format('dddd, MMMM Do YYYY')}</td>
              <td>{Moment(professionalLearnig.endDateTime).format('dddd, MMMM Do YYYY')}</td>
              <td>{professionalLearnig.registrationExpenses}</td>
              <td>{professionalLearnig.ownedBy}</td>
              <td>{professionalLearnig.isExternal}</td>
              <td>{professionalLearnig.accommodationExpenses}</td>
              <td>{professionalLearnig.travelExpenses}</td>
              <td>{professionalLearnig.expenseComment}</td>
              <td>
                <Link type="button" className="btn btn-primary" to={`/professionallearning/${professionalLearnig.id}`}>Edit</Link>&nbsp;
              <button type="button" className="btn btn-danger" onClick={props.handleDeleteClick.bind(null, professionalLearnig.id)}>Delete</button>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}


function renderPagination(props) {
  const prevStartDateIndex = (props.startDateIndex || 0) - 5;
  const nextStartDateIndex = (props.startDateIndex || 0) + 5;

  return <p className='clearfix text-center'>
    <Link className='btn btn-default pull-left' to={`/${prevStartDateIndex}`}>Previous</Link>
    <Link className='btn btn-default pull-right' to={`/${nextStartDateIndex}`}>Next</Link>
    {props.isLoading ? <span>Loading...</span> : []}
  </p>;
}

export default connect(
  state => state.professionalLearning,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Home);
