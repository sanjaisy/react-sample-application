import React from 'react';
import NavMenu from './Header/NavMenu';

export default props => (
  <div>
    <NavMenu />
    <Container-fluid>
      {props.children}
    </Container-fluid>
  </div>
);
