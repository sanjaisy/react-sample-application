import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../../store/EventStore';

class EventsAction extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        // This method is called when the component is first added to the document
        this.ensureDataFetched();
    }

    ensureDataFetched() {
        this.id = this.props.match.params.id;
        if (this.id == undefined || this.id == null) {
            this.props.resetEvent();
        } else {
            this.props.requestEvent(this.id);
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        await this.props.eventSubmit(this.id).then(rest => {
            this.props.history.push('/event');
        });
        
    }

    render() {
        return (
            <div className="container mb-5">
                <h1>Edit/Add Event</h1>
                <p>This component demonstrates Add/Edit data from the server and working with URL parameters.</p>
                <br /><br />
                <RenderEventsCards {...this.props} handleSubmit={this.handleSubmit} />
            </div>
        );
    }
}


function RenderEventsCards(props) {
    return (
        <form className="container" onSubmit={props.handleSubmit}>
            <div className="form-row">
                <div className="form-group col-sm-4">
                    <label>Event Name *</label>
                    <input type="text" className="form-control" name="eventName" value={props.Event.eventName || ''} onChange={props.updateEventValue}
                        aria-describedby="Event Name" placeholder="Enter a Event Name" />
                </div>
                <div className="form-group col-sm-4">
                    <label>Event Description *</label>
                    <textarea className="form-control" name="eventDescription" value={props.Event.eventDescription || ''} onChange={props.updateEventValue}
                    aria-describedby="Event Short Description" placeholder="Enter a Event Description" />
                </div>
                <div className="form-group col-sm-4">
                    <label>Event Short Description *</label>
                    <textarea className="form-control" name="eventShortDescription" value={props.Event.eventShortDescription || ''} onChange={props.updateEventValue}
                    aria-describedby="Event Short Description" placeholder="Enter an Event Short Description" />
                </div>
            </div >
            <br />

            <div className="form-row">
                <div className="form-group col-sm-4">
                    <label>Event Start DateTime *</label>
                    <input type="text" className="form-control" name="eventStartDateTime" value={props.Event.eventStartDateTime || ''} onChange={props.updateEventValue}
                        aria-describedby="Event Start Date Time" placeholder="Enter a Event Start Date Time" />
                </div>
                <div className="form-group col-sm-4">
                    <label>Event End Date Time *</label>
                    <input className="form-control" name="eventEndDateTime" value={props.Event.eventEndDateTime || ''} onChange={props.updateEventValue}
                    aria-describedby="Event EndDate Time" placeholder="Enter a Event End Date Time" />
                </div>
                <div className="form-group col-sm-4">
                    <label>Web Url *</label>
                    <input className="form-control" name="webUrl" value={props.Event.webUrl || ''} onChange={props.updateEventValue}
                    aria-describedby="Web Url" placeholder="Enter an WebUrl" />
                </div>
            </div >
            <br />

            <div className="form-row">
                <div className="form-group col-sm-4">
                    <label>Attendee Limit *</label>
                    <input type="text" className="form-control" name="attendeeLimit" value={props.Event.attendeeLimit || ''} onChange={props.updateEventValue}
                        aria-describedby="Attendee Limit" placeholder="Enter Attendee Limit" />
                </div>
                <div className="form-group col-sm-4">
                    <label>Contact Name *</label>
                    <input className="form-control" name="contactName" value={props.Event.contactName || ''} onChange={props.updateEventValue}
                    aria-describedby="Contact Name" placeholder="Enter Contact Name" />
                </div>
                <div className="form-group col-sm-4">
                    <label>Contact Email *</label>
                    <input className="form-control" name="contactEmail" value={props.Event.contactEmail || ''} onChange={props.updateEventValue}
                    aria-describedby="Web Url" placeholder="Enter a Contact Email" />
                </div>
            </div >
            <br />


            <div className="form-row">
                <div className="form-group col-sm-4">
                    <label>Contact Phone *</label>
                    <input type="text" className="form-control" name="contactPhone" value={props.Event.contactPhone || ''} onChange={props.updateEventValue}
                        aria-describedby="Contact Phone" placeholder="Enter Contact Phone" />
                </div>
                <div className="form-group col-sm-4">
                    <label>Presenter *</label>
                    <input className="form-control" name="presenter" value={props.Event.presenter || ''} onChange={props.updateEventValue}
                    aria-describedby="Contact Name" placeholder="Enter Presenter Name" />
                </div>
                <div className="form-group col-sm-4">
                    <label>Cost *</label>
                    <input className="form-control" name="cost"  value={props.Event.cost || ''} onChange={props.updateEventValue}
                    aria-describedby="Web Url" placeholder="Enter a Cost" />
                </div>
            </div >
            <div className="d-flex justify-content-end">
                <Link type="button" className='btn btn-secondary' to={`/event`}>Cancel</Link> &nbsp;&nbsp;
                <input type="submit" className='btn btn-primary' value="Submit" />
            </div>

        </form >
    );
}

export default connect(
    state => state.EventManagement,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(EventsAction);
