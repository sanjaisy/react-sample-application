import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../../store/EventStore';
import Moment from 'moment';

class Counter extends Component {
  constructor(props) {
    super(props);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
  }
  componentDidMount() {
    // This method is called when the component is first added to the document
    this.ensureDataFetched();
  }
  ensureDataFetched() {
    const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
    this.props.requestEvents(startDateIndex);
  }
  handleDeleteClick= async (id) => {
    await this.props.deleteEvent(id);
    this.ensureDataFetched();
  }

  render() {
    return (
      <div>
        <h1>Events</h1>
        <p>This component demonstrates fetching data from the server and working with URL parameters.</p>
        <RenderEventsTable {...this.props} handleDeleteClick={this.handleDeleteClick} />
        {renderPagination(this.props)}
      </div>
    );
  }
}


function RenderEventsTable(props) {
  return (
    <div>
      <div className="d-flex flex-row-reverse bd-highlight p-2">
        <Link type="button" className="btn btn-primary" to={`/actionEvent`}>Add Events</Link>
      </div>
      <br /><br />
      <div className="d-flex justify-content-between m-4">
        {props.Events.map(events =>
          <div className="col-sm-4">
            <div className="card text-center">
              <div className="card-header card-title">
                {events.eventName}
              </div>
              <div className="card-body">
                <div class="form-group row">
                  <strong for="staticEmail" class="col-sm-6 col-form-label">Event Description :</strong>
                  <div class="col-sm-6">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value={events.eventDescription} />
                  </div>
                </div>
                <div class="form-group row">
                  <strong for="staticEmail" class="col-sm-6 col-form-label">Event Start Date/Time :</strong>
                  <div class="col-sm-6">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value={Moment(events.eventStartDateTime).format('dddd, MMMM Do YYYY')} />
                  </div>
                </div>
                <div class="form-group row">
                  <strong for="staticEmail" class="col-sm-6 col-form-label">Event End Date/Time :</strong>
                  <div class="col-sm-6">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value={Moment(events.eventEndDateTime).format('dddd, MMMM Do YYYY')} />
                  </div>
                </div>
                <div class="form-group row">
                  <strong for="staticEmail" class="col-sm-6 col-form-label">Presenter:</strong>
                  <div class="col-sm-6">
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value={events.presenter} />
                  </div>
                </div>

              </div>
              <div className="card-footer text-muted">
                <div className="d-flex justify-content-between">
                  <Link className='btn btn-primary pull-left' to={`/actionEvent/${events.id}`}>Edit</Link>
                  <button className='btn btn-primary pull-right' onClick={props.handleDeleteClick.bind(null, events.id)}>Delete</button>
                </div>
              </div>
            </div>
          </div>

        )}
      </div>

    </div>
  );
}

function renderPagination(props) {
  const prevStartDateIndex = (props.startDateIndex || 0) - 5;
  const nextStartDateIndex = (props.startDateIndex || 0) + 5;

  return <p className='clearfix text-center'>
    <Link className='btn btn-default pull-left' to={`/fetch-data/${prevStartDateIndex}`}>Previous</Link>
    <Link className='btn btn-default pull-right' to={`/fetch-data/${nextStartDateIndex}`}>Next</Link>
    {props.isLoading ? <span>Loading...</span> : []}
  </p>;
}

export default connect(
  state => state.EventManagement,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Counter);
