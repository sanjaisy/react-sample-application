import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../../store/UserManagementStore';

class FetchData extends Component {

  constructor(props) {
    super(props);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
  }
  

  componentDidMount() {
    // This method is called when the component is first added to the document
    this.ensureDataFetched();
  }


  handleDeleteClick= async (id) => {
    await this.props.deleteUserManagement(id);
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    const startDateIndex = parseInt(this.props.match.params.startDateIndex, 10) || 0;
    this.props.requestWeatherForecasts(startDateIndex);
  }

  render() {
    return (
      <div>
        <h1>User  Management</h1>
        <p>This component demonstrates fetching data from the server and working with URL parameters.</p>
        <RenderForecastsTable {...this.props} handleDeleteClick={this.handleDeleteClick} />
        {renderPagination(this.props)}
      </div>
    );
  }
}

function RenderForecastsTable(props) {
  return (
    <div>
      <div className="d-flex flex-row-reverse bd-highlight p-2">
        <Link type="button" className="btn btn-primary" to={`/usermanagement`}>Add User</Link>
      </div>

      <table className='table table-striped'>
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {props.users.map(user =>
            <tr key={user.id}>
              <td>{user.firstName}</td>
              <td>{user.lastName}</td>
              <td>{user.email}</td>
              <td>
                <Link type="button" className="btn btn-primary" to={`/usermanagement/${user.id}`}>Edit</Link>&nbsp;
              <button type="button" className="btn btn-danger" onClick={props.handleDeleteClick.bind(null, user.id)}>Delete</button>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}

function renderPagination(props) {
  const prevStartDateIndex = (props.startDateIndex || 0) - 5;
  const nextStartDateIndex = (props.startDateIndex || 0) + 5;

  return <p className='clearfix text-center'>
    <Link className='btn btn-default pull-left' to={`/fetch-data/${prevStartDateIndex}`}>Previous</Link>
    <Link className='btn btn-default pull-right' to={`/fetch-data/${nextStartDateIndex}`}>Next</Link>
    {props.isLoading ? <span>Loading...</span> : []}
  </p>;
}

export default connect(
  state => state.UserManagement,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(FetchData);
