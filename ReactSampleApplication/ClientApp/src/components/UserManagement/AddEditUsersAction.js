import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../../store/UserManagementStore';
import FormValidator from '../../Common/FormValidator';

class UsermanagementAction extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

        this.validator = new FormValidator([
            {
                field: 'email',
                method: 'isEmpty',
                validWhen: false,
                message: 'Email is required.'
            },
            {
                field: 'email',
                method: 'isEmail',
                validWhen: true,
                message: 'That is not a valid email.'
            },
            {
                field: 'firstName',
                method: 'isEmpty',
                validWhen: false,
                message: 'Pleave provide a First Name.'
            },
            {
                field: 'lastName',
                method: 'isEmpty',
                validWhen: false,
                message: 'Pleave provide a Lirst Name.'
            },
        ]);

        this.state = {
            email: '',
            firstName: '',
            lastName: '',
            validation: this.validator.valid()
        }
        this.submitted = false;
    }
    componentDidMount() {
        // This method is called when the component is first added to the document
        this.ensureDataFetched();
    }

    ensureDataFetched() {
        this.id = this.props.match.params.id;
        if (this.id == undefined || this.id == null) {
            this.setState({
                email: '',
                firstName: '',
                lastName: '',
                validation: this.validator.valid()
            });
        } else {
            this.props.filterUserManagementById(this.id).then(res => {    
                this.setState(res);
                });
        }
    }

    handleInputChange = event => {
        event.preventDefault();

        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if (validation.isValid) {
            await this.props.UserManagementFormsubmit(this.id, this.state);
            this.props.history.push('/fetch-data');
        }


    }

    render() {
        let validation = this.submitted ?                         // if the form has been submitted at least once
            this.validator.validate(this.state) :                 // then check validity every time we render
            this.state.validation                                  // otherwise just use what's in state

        return (
            <div className="container mb-5">
                <h1>Edit/Add User</h1>
                <p>This component demonstrates Add/Edit data from the server and working with URL parameters.</p>
                <br /><br />

                <form className="container" onSubmit={this.handleSubmit}>
                    <div className="form-row">
                        <div className={validation.firstName.isInvalid ? 'has-error form-group col-sm-4' : 'form-group col-sm-4'}>
                            <label>First Name *</label>
                            <input type="text" className="form-control" name="firstName" value={this.state.firstName || ''} onChange={this.handleInputChange}
                                aria-describedby="First Name" placeholder="Enter a first Name" />
                            <span className="help-block">{validation.firstName.message}</span>
                        </div>
                        <div className={validation.lastName.isInvalid ? 'has-error form-group col-sm-4' : 'form-group col-sm-4'}>
                            <label>Last Name *</label>
                            <input type="text" className="form-control" name="lastName" value={this.state.lastName || ''} onChange={this.handleInputChange}
                                aria-describedby="lastName" placeholder="Enter a Last Name" />
                            <span className="help-block">{validation.lastName.message}</span>
                        </div>
                        <div className={validation.email.isInvalid ? 'has-error form-group col-sm-4' : 'form-group col-sm-4'}>
                            <label>Email *</label>
                            <input type="text" className="form-control" name="email" value={this.state.email || ''} onChange={this.handleInputChange}
                                aria-describedby="Provider" placeholder="Enter an email" />
                            <span className="help-block">{validation.email.message}</span>
                        </div>
                    </div >
                    <br />
                    <div className="d-flex justify-content-end">
                        <Link type="button" className='btn btn-secondary' to={`/fetch-data`}>Cancel</Link> &nbsp;&nbsp;
                <input type="submit" className='btn btn-primary' value="Submit" />
                    </div>
                </form >


            </div>
        );
    }
}

export default connect(
    state => state.UserManagement,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(UsermanagementAction);