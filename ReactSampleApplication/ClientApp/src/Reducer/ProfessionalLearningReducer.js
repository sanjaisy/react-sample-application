import ReduxConstants from '../Common/ReduxConstants';

const initialState = { professionalLearnings: [], professionalLearning: {}, isLoading: false };

export const reducer = (state, action) => {
    state = state || initialState;

    switch (action.type) {
        case ReduxConstants.requestProfessionalLearningType:
            return {
                ...state,
                startDateIndex: action.startDateIndex,
                isLoading: true
            };

        case ReduxConstants.receiveProfessionalLearningType:
            return {
                ...state,
                startDateIndex: action.startDateIndex,
                professionalLearnings: action.professionalLearnings,
                isLoading: false
            };

        case ReduxConstants.requestFilterProfessionalLearningTypeId:
            return {
                ...state,
                professionalLearning:{},
                isLoading: true
            };


        case ReduxConstants.receiveFilterProfessionalLearningId:
            return {
                ...state,
                professionalLearning: action.professionalLearning,
                isLoading: false
            };


        case ReduxConstants.recieveProfessionalLearningUpdatecValue:
            return {
                ...state,
                professionalLearning: Object.assign({},action.professionalLearning),
                isLoading: false
            };
        default:
            return state;
    }
};